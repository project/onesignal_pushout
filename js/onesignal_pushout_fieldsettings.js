(function ($, Drupal)
{
  Drupal.behaviors.onesignal_pushout_fieldsettings = {
    attach: function (context, settings)
    {
      if($('#edit-settings-scheduled-pushout').is(':checked')) {
        $('div.js-form-item-settings-scheduled-pushout-label').show();
      } else {
        $('div.js-form-item-settings-scheduled-pushout-label').hide();
      }

      $('#edit-settings-scheduled-pushout').click(function() {
        if($(this).is(':checked')) {
          $('div.js-form-item-settings-scheduled-pushout-label').show();
        } else {
          $('div.js-form-item-settings-scheduled-pushout-label').hide();
        }
      });
    }
  };
}(jQuery, Drupal));