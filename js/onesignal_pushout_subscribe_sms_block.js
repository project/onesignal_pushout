(function ($, Drupal, once)
{
  $.fn.onEnter = function(func) {
    this.bind('keypress', function(e) {
      if (e.keyCode == 13) func.apply(this, [e]);
    });

    return this;
  };

  function validateSubscribeSMS(message = null) {
    var formValid = true;

    $subscribeSMSBlockForm = $('form.onesignal-pushout-subscribe-sms-block-form');

    if($subscribeSMSBlockForm.length == 1) {
      $subscribeSMSBlockForm.find('.details-success-message').remove();
      $subscribeSMSBlockForm.find('.details-error-message').remove();

      $subscribeSMSBlockForm.find('.subscribe-sms-phone-number').parent().removeClass('form-item--error');
      $subscribeSMSBlockForm.find('.subscribe-sms-phone-number').removeClass('error');

      if(formValid == true) {
        if(!(formValid = $subscribeSMSBlockForm.find('.subscribe-sms-phone-number').val().trim() != "")) {
          // Add error message
          $subscribeSMSBlockForm.prepend('<div class="details-error-message">Enter phone number to subscribe.</div>');

          $subscribeSMSBlockForm.find('.subscribe-sms-phone-number').parent().addClass('form-item--error');
          $subscribeSMSBlockForm.find('.subscribe-sms-phone-number').addClass('error');
        }
      }

      if(formValid == true) {
        //TODO: Clean and validate phone number
        //text.match(/price\[(\d+)\]\[(\d+)\]/);
      }

      if(formValid == true) {
        if(!(formValid = message == null)) {
          // Add error message
          $subscribeSMSBlockForm.prepend('<div class="details-error-message">' + Drupal.checkPlain(message) + '</div>');
        }
      }
    }

    return formValid;
  }

  function submitSubscribeSMS(message = null) {
    var formValid = validateSubscribeSMS();

    if(formValid) {
      // Empty phone number
      $subscribeSMSBlockForm.find('.subscribe-sms-phone-number').val("");

      if(message != null) {
        $subscribeSMSBlockForm.prepend('<div class="details-success-message">' + Drupal.checkPlain(message) + '</div>');
      }
    }
  }

  Drupal.behaviors.onesignalPushoutSubscribeSMSBlockForm = {
    attach: function(context, settings)
    {
      //TODO: Add mouse down.
      // $(context).find('form.onesignal-pushout-subscribe-sms-block-form').once('onesignalPushoutSubscribeSMSBlockForm').each(function() {
      //   $subscribeSMSBlockForm.find('.subscribe-sms-phone-number').onEnter(function() {
      //     $subscribeSMSBlockForm.find('.subscribe-sms-submit').mousedown();
      //   });
      // });

    },
  };

  // Argument passed from InvokeCommand.
  $.fn.validateSubscribeSMSCallback = function(message) {
    validateSubscribeSMS(message);
  };

  $.fn.submitSubscribeSMSCallback = function(message) {
    submitSubscribeSMS(message);
  };
})(jQuery, Drupal, once);
