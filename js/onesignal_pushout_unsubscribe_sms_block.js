(function ($, Drupal, once)
{
  $.fn.onEnter = function(func) {
    this.bind('keypress', function(e) {
      if (e.keyCode == 13) func.apply(this, [e]);
    });

    return this;
  };

  function validateUnsubscribeSMS(message = null) {
    var formValid = true;

    $subscribeSMSBlockForm = $('form.onesignal-pushout-unsubscribe-sms-block-form');

    if($subscribeSMSBlockForm.length == 1) {
      $subscribeSMSBlockForm.find('.details-success-message').remove();
      $subscribeSMSBlockForm.find('.details-error-message').remove();

      $subscribeSMSBlockForm.find('.unsubscribe-sms-phone-number').parent().removeClass('form-item--error');
      $subscribeSMSBlockForm.find('.unsubscribe-sms-phone-number').removeClass('error');

      if(formValid == true) {
        if(!(formValid = $subscribeSMSBlockForm.find('.unsubscribe-sms-phone-number').val().trim() != "")) {
          // Add error message
          $subscribeSMSBlockForm.prepend('<div class="details-error-message">Enter phone number to unsubscribe.</div>');

          $subscribeSMSBlockForm.find('.unsubscribe-sms-phone-number').parent().addClass('form-item--error');
          $subscribeSMSBlockForm.find('.unsubscribe-sms-phone-number').addClass('error');
        }
      }

      if(formValid == true) {
        //TODO: Clean and validate phone number
        //text.match(/price\[(\d+)\]\[(\d+)\]/);
      }

      if(formValid == true) {
        if(!(formValid = message == null)) {
          // Add error message
          $subscribeSMSBlockForm.prepend('<div class="details-error-message">' + Drupal.checkPlain(message) + '</div>');
        }
      }
    }

    return formValid;
  }

  function submitUnsubscribeSMS(message = null) {
    var formValid = validateUnsubscribeSMS();

    if(formValid) {
      // Empty phone number
      $subscribeSMSBlockForm.find('.unsubscribe-sms-phone-number').val("");

      if(message != null) {
        $subscribeSMSBlockForm.prepend('<div class="details-success-message">' + Drupal.checkPlain(message) + '</div>');
      }
    }
  }

  Drupal.behaviors.onesignalPushoutSubscribeSMSBlockForm = {
    attach: function(context, settings)
    {
      //TODO: Add mouse down.
      // $(context).find('form.onesignal-pushout-subscribe-sms-block-form').once('onesignalPushoutSubscribeSMSBlockForm').each(function() {
      //   $subscribeSMSBlockForm.find('.subscribe-sms-phone-number').onEnter(function() {
      //     $subscribeSMSBlockForm.find('.subscribe-sms-submit').mousedown();
      //   });
      // });

    },
  };

  // Argument passed from InvokeCommand.
  $.fn.validateUnsubscribeSMSCallback = function(message) {
    validateUnsubscribeSMS(message);
  };

  $.fn.submitUnsubscribeSMSCallback = function(message) {
    submitUnsubscribeSMS(message);
  };
})(jQuery, Drupal, once);