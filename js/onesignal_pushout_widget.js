(function ($, Drupal)
{
  Drupal.behaviors.onesignal_pushout_widget = {
    attach: function (context, settings)
    {
      $('.pushnow-checkbox').click(function() {
        if($(this).is(':checked')) {
          $('.pushlater-checkbox').prop('checked', false);
          $('.pushlater-datetime').parent('.form-datetime-wrapper').hide();
        }
      });

      if($('.pushlater-checkbox').is(':checked')) {
        $('.pushlater-datetime').parent('.form-datetime-wrapper').show();
      } else {
        $('.pushlater-datetime').parent('.form-datetime-wrapper').hide();
      }

      $('.pushlater-checkbox').click(function() {
        if($(this).is(':checked')) {
          $('.pushnow-checkbox').prop('checked', false);
          $('.pushlater-datetime').parent('.form-datetime-wrapper').show();
        } else {
          $('.pushlater-datetime').parent('.form-datetime-wrapper').hide();
        }
      });
    }
  };
}(jQuery, Drupal));