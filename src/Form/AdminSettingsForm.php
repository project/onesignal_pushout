<?php

namespace Drupal\onesignal_pushout\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Logger\LoggerChannel;
use Upanupstudios\OneSignal\Php\Client\Config;
use Upanupstudios\OneSignal\Php\Client\OneSignal;
use Symfony\Component\DependencyInjection\ContainerInterface;
use GuzzleHttp\Client;

/**
 * Configure OneSignal Pushout settings.
 */
class AdminSettingsForm extends ConfigFormBase {

  /**
   * Messenger service.
   *
   * @var Drupal\Core\Logger\LoggerChannel
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public function __construct(LoggerChannel $logger) {
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('logger.factory')->get('onesignal_pushout')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'onesignal_pushout_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['onesignal_pushout.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Default settings
    $settings = $this->config('onesignal_pushout.settings');

    $app_name = $settings->get('app_name');
    $app_id = $settings->get('app_id');
    $api_key = $settings->get('api_key');
    $apps_url = Url::fromUri('https://app.onesignal.com/apps', array('attributes' => array('target' => '_blank')));

    // API
    $form['api'] = [
      '#type' => 'details',
      '#title' => $this->t('API'),
      '#open' => empty($api_key)
    ];
    $form['api']['app_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('App Name'),
      '#default_value' => $app_name,
    ];
    $form['api']['app_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('App ID'),
      '#default_value' => $app_id,
      '#description' => $this->t('The App ID for your app in OneSignal. Get the App ID from the @appslink.', [
        '@appslink' => Link::fromTextAndUrl($this->t('App Settings page'), $apps_url)->toString()
      ])
    ];
    $form['api']['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#default_value' => $api_key,
      '#description' => $this->t('The API Key for your app in OneSignal. Get the API Key from the @appslink.', [
        '@appslink' => Link::fromTextAndUrl($this->t('App Settings page'), $apps_url)->toString()
      ])
    ];

    if(!empty($api_key)) {
      $debug_sendto_segment = $settings->get('debug_sendto_segment');

      $form['debug'] = [
        '#type' => 'details',
        '#title' => $this->t('Debug'),
        '#open' => !empty($debug_sendto_segment),
      ];
      $form['debug']['debug_sendto_segment'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Send to segment'),
        '#default_value' => $debug_sendto_segment,
        '#description' => $this->t('Send all push notifications to this segment for testing.'),
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $app_id = $form_state->getValue('app_id');
    $api_key = $form_state->getValue('api_key');

    $config = new Config($app_id, $api_key);
    $httpClient = new Client();
    $oneSignal = new OneSignal($config, $httpClient);

    // View the app as a test
    $response = $oneSignal->apps()->view($app_id);

    if(empty($response['id']) || (!empty($response['id']) && $response['id'] != $app_id)) {
      $form_state->setErrorByName('api_token', $this->t($response));
      $this->logger->error($response);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $settings = $this->config('onesignal_pushout.settings');

    $settings->set('app_name', $form_state->getValue('app_name'))
      ->set('app_id', $form_state->getValue('app_id'))
      ->set('api_key', $form_state->getValue('api_key'))
      ->save();

    $debug_sendto_segment = $form_state->getValue('debug_sendto_segment');

    $settings->set('debug_sendto_segment', $debug_sendto_segment)
      ->save();

    return parent::submitForm($form, $form_state);
  }

}
