<?php

namespace Drupal\onesignal_pushout\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Upanupstudios\OneSignal\Php\Client\Config;
use Upanupstudios\OneSignal\Php\Client\OneSignal;
use GuzzleHttp\Client;
use Drupal\Core\Logger\LoggerChannel;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Builds the subscribe form for the subscribe block.
 */
class SubscribeSMSBlockForm extends FormBase {

  /**
   * Logger service.
   *
   * @var Drupal\Core\Logger\LoggerChannel
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public function __construct(LoggerChannel $logger) {
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('logger.factory')->get('onesignal_pushout')
    );
  }

  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'onesignal_pushout_subscribe_sms_block_form';
  }

  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $machine_name = null) {

    $unsubscribe = null;

    if(!empty($machine_name)) {
      $block = \Drupal\block\Entity\Block::load($machine_name);

      if (!empty($block)) {
        $settings = $block->get('settings');

        if(!empty($settings['unsubscribe'])) {
          $unsubscribe = $settings['unsubscribe'];
        }
      }
    }

    $form['machine_name'] = [
      '#type' => 'hidden',
      '#value' => $machine_name
    ];

    $form['phone_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cell Phone Number'),
      '#placeholder' => $this->t('2501234567'),
      '#required' => TRUE,
      '#attributes' => [
        'novalidate' => 'novalidate',
        'class' => ['subscribe-sms-phone-number'],
      ]
    ];

    $form['#attached']['library'][] = 'onesignal_pushout/onesignal_pushout_subscribe_sms_block';

    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#id' => 'subscribesubmit',
        '#type' => 'submit',
        '#value' => $this->t('Submit'),
        '#attributes' => [
          'class' => ['subscribe-sms-submit'],
        ],
        '#ajax' => [
          'callback' => array($this, 'submitFormCallback'),
          'event' => 'click',
        ],
      ],
    ];

    if(!empty($unsubscribe)) {
      $form['unsubscribe'] = [
        '#markup' => $unsubscribe
      ];
    }

    // Remove progress message
    $form['actions']['submit']['#ajax']['progress']['message'] = '';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    //TODO: Validate phone number
    $phone_number = $form_state->getValue('phone_number');

    $valid = $this->validatePhoneNumber($phone_number);

    if(empty($valid)) {
      $form_state->setErrorByName('phone_number', $this->t('Invalid cell phone number.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Uses Ajax to submit data using submitFormCallback function

    $machine_name = $form_state->getValue('machine_name');
    $phone_number = $form_state->getValue('phone_number');

    $success_message = 'You are now subscribed to SMS.';

    $block = \Drupal\block\Entity\Block::load($machine_name);

    if (!empty($block)) {
      $settings = $block->get('settings');

      if(!empty($settings['success_message'])) {
        $success_message = $settings['success_message'];
      }
    }

    $settings = \Drupal::config('onesignal_pushout.settings');
    $app_id = $settings->get('app_id');
    $api_key = $settings->get('api_key');

    if(!empty($app_id) && !empty($api_key)) {
      $config = new Config($app_id, $api_key);
      $httpClient = new Client();
      $oneSignal = new OneSignal($config, $httpClient);

      if(!preg_match('/^\+1/', $phone_number)) {
        $phone_number = '+1'.$phone_number;
      }

      $data = [
        'app_id' => $app_id,
        'device_type' => 14,
        'identifier' => $phone_number,
        'language' => 'en'
      ];

      $response = $oneSignal->devices()->add($data);

      if(!empty($response['id']) && !empty($response['success'])) {
        $message = t('@phone_number has been subscribed to SMS.', ['@phone_number' => $phone_number]);
        \Drupal::messenger()->addMessage($message);
        $this->logger->notice($message);
      } else {
        $form_state->setErrorByName('phone_number', $response);
        $this->logger->error($response);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitFormCallback(array &$form, FormStateInterface $form_state) {
    $ajaxResponse = new AjaxResponse();

    $machine_name = $form_state->getValue('machine_name');
    $phone_number = $form_state->getValue('phone_number');

    $success_message = 'You are now subscribed to SMS.';

    $block = \Drupal\block\Entity\Block::load($machine_name);

    if (!empty($block)) {
      $settings = $block->get('settings');

      if(!empty($settings['success_message'])) {
        $success_message = $settings['success_message'];
      }
    }

    // Validation
    if(empty($phone_number)) {
      $ajaxResponse->addCommand(
        new InvokeCommand(NULL, 'validateSubscribeSMSCallback', [$this->t('Enter cell phone number to subscribe.')])
      );
    } else {
      // Validate phone number
      $valid = $this->validatePhoneNumber($phone_number);

      if(!empty($valid)) {
        $settings = \Drupal::config('onesignal_pushout.settings');
        $app_id = $settings->get('app_id');
        $api_key = $settings->get('api_key');

        if(!empty($app_id) && !empty($api_key)) {
          $config = new Config($app_id, $api_key);
          $httpClient = new Client();
          $oneSignal = new OneSignal($config, $httpClient);

          if(!preg_match('/^\+1/', $phone_number)) {
            $phone_number = '+1'.$phone_number;
          }

          $data = [
            'app_id' => $app_id,
            'device_type' => 14,
            'identifier' => $phone_number,
            'language' => 'en'
          ];

          $response = $oneSignal->devices()->add($data);

          if(!empty($response['id']) && !empty($response['success'])) {
            $this->logger->notice(t('@phone_number has been subscribed to SMS.', ['@phone_number' => $phone_number]));

            $ajaxResponse->addCommand(
              new InvokeCommand(NULL, 'submitSubscribeSMSCallback', [$this->t($success_message)])
            );
          } else {
            $this->logger->error($response);

            $ajaxResponse->addCommand(
              new InvokeCommand(NULL, 'validateSubscribeSMSCallback', [$this->t($response)])
            );
          }
        }
      } else {
        $ajaxResponse->addCommand(
          new InvokeCommand(NULL, 'validateSubscribeSMSCallback', [$this->t('Invalid cell phone number.')])
        );
      }
    }

    return $ajaxResponse;
  }

  public function validatePhoneNumber($phone_number) {
    // Replace non-numeric characters
    $phone_number = trim(preg_replace('/\D/', '', $phone_number));

    $regex = '/^(?:(\()?\d{3}(?(1)\))[-. ]?\d{3}[-. ]?\d{4})$/';

    if(preg_match($regex, $phone_number)) {
      return $phone_number;
    }

    return false;
  }
}