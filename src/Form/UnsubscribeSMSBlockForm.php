<?php

namespace Drupal\onesignal_pushout\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Upanupstudios\OneSignal\Php\Client\Config;
use Upanupstudios\OneSignal\Php\Client\OneSignal;
use GuzzleHttp\Client;
use Drupal\Core\Logger\LoggerChannel;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Builds the unsubscribe form for the unsubscribe block.
 */
class UnsubscribeSMSBlockForm extends FormBase {

  /**
   * Logger service.
   *
   * @var Drupal\Core\Logger\LoggerChannel
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public function __construct(LoggerChannel $logger) {
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('logger.factory')->get('onesignal_pushout')
    );
  }

  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'onesignal_pushout_unsubscribe_sms_block_form';
  }

  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $machine_name = null) {

    $form['machine_name'] = [
      '#type' => 'hidden',
      '#value' => $machine_name
    ];

    $form['phone_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cell Phone Number'),
      '#placeholder' => $this->t('2501234567'),
      '#required' => TRUE,
      '#attributes' => [
        'novalidate' => 'novalidate',
        'class' => ['unsubscribe-sms-phone-number'],
      ]
    ];

    $form['#attached']['library'][] = 'onesignal_pushout/onesignal_pushout_unsubscribe_sms_block';

    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#id' => 'unsubscribesubmit',
        '#type' => 'submit',
        '#value' => $this->t('Submit'),
        '#attributes' => [
          'class' => ['unsubscribe-sms-submit'],
        ],
        '#ajax' => [
          'callback' => array($this, 'submitFormCallback'),
          'event' => 'click',
        ],
      ],
    ];

    // Remove progress message
    $form['actions']['submit']['#ajax']['progress']['message'] = '';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Uses Ajax to submit data using submitFormCallback function
  }

  /**
   * {@inheritdoc}
   */
  public function submitFormCallback(array &$form, FormStateInterface $form_state) {
    $ajaxResponse = new AjaxResponse();

    $machine_name = $form_state->getValue('machine_name');
    $phone_number = $form_state->getValue('phone_number');

    $success_message = 'You are now unsubscribed from SMS.';

    if(!empty($machine_name)) {
      $block = \Drupal\block\Entity\Block::load($machine_name);

      if (!empty($block)) {
        $settings = $block->get('settings');

        if(!empty($settings['success_message'])) {
          $success_message = $settings['success_message'];
        }
      }
    }

    // Validation
    if(empty($phone_number)) {
      $ajaxResponse->addCommand(
        new InvokeCommand(NULL, 'validateUnsubscribeSMSCallback', [$this->t('Enter cell phone number to unsubscribe.')])
      );
    } else {
      // Validate phone number
      $valid = $this->validatePhoneNumber($phone_number);

      if(!empty($valid)) {
        $settings = \Drupal::config('onesignal_pushout.settings');
        $app_id = $settings->get('app_id');
        $api_key = $settings->get('api_key');

        if(!empty($app_id) && !empty($api_key)) {
          $config = new Config($app_id, $api_key);
          $httpClient = new Client();
          $oneSignal = new OneSignal($config, $httpClient);

          if(!preg_match('/^\+1/', $phone_number)) {
            $phone_number = '+1'.$phone_number;
          }

          $device = $oneSignal->devices()->getByIdentifier($app_id, $phone_number);

          if(!empty($device)) {
            $response = $oneSignal->devices()->delete($app_id, $device['id']);

            if(!empty($response['success'])) {
              $this->logger->notice(t('@phone_number has been unsubscribed from SMS.', ['@phone_number' => $phone_number]));

              $ajaxResponse->addCommand(
                new InvokeCommand(NULL, 'submitUnsubscribeSMSCallback', [$this->t($success_message)])
              );
            } else {
              $this->logger->error($response);

              $ajaxResponse->addCommand(
                new InvokeCommand(NULL, 'validateUnsubscribeSMSCallback', [$this->t($response)])
              );
            }
          } else {
            $ajaxResponse->addCommand(
              new InvokeCommand(NULL, 'validateUnsubscribeSMSCallback', [$this->t('Unable to find cell phone number.')])
            );
          }
        }
      } else {
        $ajaxResponse->addCommand(
          new InvokeCommand(NULL, 'validateUnsubscribeSMSCallback', [$this->t('Invalid cell phone number.')])
        );
      }
    }

    return $ajaxResponse;
  }

  public function validatePhoneNumber($phone_number) {
    // Replace non-numeric characters
    $phone_number = trim(preg_replace('/\D/', '', $phone_number));

    $regex = '/^(?:(\()?\d{3}(?(1)\))[-. ]?\d{3}[-. ]?\d{4})$/';

    if(preg_match($regex, $phone_number)) {
      return $phone_number;
    }

    return false;
  }
}