<?php

namespace Drupal\onesignal_pushout\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\onesignal_pushout\Form\SubscribeSMSBlockForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a Subscribe block.
 *
 * @Block(
 *   id = "onesignal_pushout_subscribe_sms_block",
 *   admin_label = @Translation("OneSignal Subscribe SMS"),
 * )
 */
class SubscribeSMSBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * Creates a SelectionsBlock instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FormBuilderInterface $form_builder) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->formBuilder = $form_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('form_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'description' => NULL,
      'success_message' => NULL,
      'unsubscribe' => NULL
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $form['description'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Description'),
      '#default_value' => $this->configuration['description']
    ];

    $form['success_message'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Success Message'),
      '#default_value' => $this->configuration['success_message']
    ];

    $form['unsubscribe'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Unsubscribe'),
      '#default_value' => $this->configuration['unsubscribe']
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);

    $values = $form_state->getValues();
    $this->configuration['description'] = $values['description']['value'];
    $this->configuration['success_message'] = $values['success_message'];
    $this->configuration['unsubscribe'] = $values['unsubscribe']['value'];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    //NOTE: Is this reliable?
    $machine_name = $this->getMachineNameSuggestion();

    $build = [
      '#theme' => 'subscribe_sms_block',
      '#subscribe_sms_block_form' => $this->formBuilder->getForm(SubscribeSMSBlockForm::class, $machine_name),
      '#data' => [
        'description' => $this->configuration['description'],
      ]
    ];

    return $build;
  }

}