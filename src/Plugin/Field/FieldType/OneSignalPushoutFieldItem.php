<?php

namespace Drupal\onesignal_pushout\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Upanupstudios\OneSignal\Php\Client\Config;
use Upanupstudios\OneSignal\Php\Client\OneSignal;
use GuzzleHttp\Client;

/**
 * Plugin implementation of the 'onesignal_pushout' field type.
 *
 * @FieldType(
 *   id = "onesignal_pushout",
 *   label = @Translation("OneSignal pushout"),
 *   description = @Translation("Allows an entity to send a push notification through OneSignal API."),
 *   default_widget = "onesignal_pushout_default"
 * )
 */
class OneSignalPushoutFieldItem extends FieldItemBase {

  protected $pushout_now = FALSE;
  protected $pushout_schedule = FALSE;

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return array(
    ) + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return array(
      'included_segments' => '',
      'excluded_segments' => '',
      'select_segments' => FALSE,
      'pushnow_label' => 'Immediately send push notifications',
      'scheduled_pushout' => FALSE,
      'scheduled_pushout_label' => 'Schedule sending push notifications'
    ) + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   * @todo: Not sure what to use this for...
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['pushout_recipients'] = DataDefinition::create('integer')
      ->setLabel(t('Pushout recipients'))
      ->setDescription(t('Number of recipients of push notification.'));

    $properties['pushout_sent'] = DataDefinition::create('integer')
      ->setLabel(t('Pushout sent'))
      ->setDescription(t('Timestamp of when push notification was sent.'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   * @todo: Not sure what to use this for...
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'pushout_recipients' => [ //TODO: Change to number of recipient
          'type' => 'int',
          'size' => 'normal',
          'not null' => TRUE,
          'default' => 0,
        ],
        'pushout_sent' => [ //TODO: Change to last date sent (timestamp)
          'type' => 'int',
          'size' => 'normal',
          'not null' => TRUE,
          'default' => 0,
        ],
      ]
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $element = parent::storageSettingsForm($form, $form_state, $has_data);

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::fieldSettingsForm($form, $form_state);

    $settings = \Drupal::config('onesignal_pushout.settings');
    $api_key = $settings->get('api_key');

    if(!empty($api_key)) {
      $onesignal_url = Url::fromUri('https://app.onesignal.com/apps', array('attributes' => array('target' => '_blank')));

      $element['included_segments'] = array(
        '#type' => 'textarea',
        '#title' => $this->t('Included Segments'),
        '#multiple' => TRUE,
        '#description' => $this->t('List the segment names you want to target. One segment name per line. Users in these segments will receive a notification. Manage segments in @OneSignal.', [
          '@OneSignal' => Link::fromTextAndUrl('OneSignal', $onesignal_url)->toString()
        ]),
        '#default_value' => $this->getSetting('included_segments'),
      );
      // $element['excluded_segments'] = array(
      //   '#type' => 'textarea',
      //   '#title' => $this->t('Excluded Segments'),
      //   '#multiple' => TRUE,
      //   '#description' => $this->t('List the segment names that will be excluded when sending. One segment name per line. Users in these segments will not receive a notification, even if they were included in included_segments. Manage segments in @OneSignal.', [
      //     '@OneSignal' => Link::fromTextAndUrl('OneSignal', $onesignal_url)->toString()
      //   ]),
      //   '#default_value' => $this->getSetting('excluded_segments'),
      // );
      $element['select_segments'] = array(
        '#title' => 'Enable to select segments (from included segments) before sending',
        '#type' => 'checkbox',
        '#default_value' => $this->getSetting('select_segments'),
      );
      $element['pushnow_label'] = array(
        '#title' => 'Push now label',
        '#type' => 'textfield',
        '#default_value' => $this->getSetting('pushnow_label'),
      );
      $element['scheduled_pushout'] = array(
        '#title' => $this->t('Enable scheduled pushout'),
        '#type' => 'checkbox',
        '#default_value' => $this->getSetting('scheduled_pushout'),
        '#attributes' => [
          'class' => [
            'scheduled-pushout-checkbox',
          ],
        ],
        '#attached' => [
          'library' => [
            'onesignal_pushout/onesignal_pushout_fieldsettings'
          ]
        ]
      );
      $element['scheduled_pushout_label'] = array(
        '#title' => 'Scheduled pushout label',
        '#type' => 'textfield',
        '#default_value' => $this->getSetting('scheduled_pushout_label'),
      );
    } else {
      $message = 'Missing OneSignal API key! Enter the API information in the @onesignal_pushout_settings.';
      $url = Url::fromRoute('onesignal_pushout.settings');

      $message = $this->t($message, [
        '@onesignal_pushout_settings' => Link::fromTextAndUrl($this->t('OneSignal settings'), $url)->toString(),
      ]);

      \Drupal::logger('onesignal_pushout')->notice($message);
      \Drupal::messenger()->addMessage($message, 'error', FALSE);
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function postSave($update) {
    parent::postSave($update);

    // Get onesignal settings
    $settings = \Drupal::config('onesignal_pushout.settings');
    $app_id = $settings->get('app_id');
    $api_key = $settings->get('api_key');
    $debug_sendto_segment = $settings->get('debug_sendto_segment');

    if(!empty($app_id) && !empty($api_key)) {
      $config = new Config($app_id, $api_key);
      $httpClient = new Client();
      $oneSignal = new OneSignal($config, $httpClient);

      $entity = $this->getEntity();
      $isPublished = $entity->isPublished();

      if($isPublished && (!empty($this->pushnow) || !empty($this->pushlater))) {
        // Entity
        $entity = clone $this->getEntity();
        $type = $entity->type->entity->id();

        if(!empty($debug_sendto_segment)) {
          // Use sendto segment
          $included_segments = array($debug_sendto_segment);

          // Empty excluded segments
          // $excluded_segments = array();
        } else {
          // Retrieve included segments
          $included_segments = array_filter(explode("\n", $this->getSetting('included_segments')));

          //TODO: Don't use excluded segments for now
          // $excluded_segments = array_filter(explode("\n", $this->getSetting('excluded_segments')));
        }

        //TODO: Figure out using filters later on
        // $filters = [];

        // Only send if either targets are not empty
        if(!empty($included_segments) || !empty($filters)) {
          $datetime = new \DateTime('now');

          if(!empty($this->pushlater)) {
            $datetime = new \DateTime($this->pushlater_datetime['object']->__toString());
          }

          $data = [
            'app_id' => $app_id,
            'headings' => [
              'en' => $type
            ],
            'contents' => [
              'en' => $entity->getTitle(),
            ],
            //TODO: What if segments are empty? Will it send to all subscribers?
            'included_segments' => $included_segments,
            // 'excluded_segments' => $excluded_segments,
            //TODO: Do not pass filters, either segments or filters should be passed, not both
            //'filters' => $filters,
            // Send with GMT to offset time in API (API defaults to UTC)
            'send_after' => $datetime->format('Y-m-d H:i:s P'),
            'data' => array(
              'id' => $entity->id(),
              'type' => $type
            ),
          ];

          $response = $oneSignal->notifications()->create($data);

          if(!empty($response['id']) && !empty($response['recipients'])) {
            if(!empty($this->pushnow)) {
              $message = 'The "@subject" has been sent to @segments segments with @recipients recipients.';
            } else {
              $message = 'The "@subject" will be sent on @date to @segments segments with @recipients recipients.';
            }

            $message = $this->t($message, array(
              '@subject' => $entity->getTitle(),
              '@date' => $datetime->format('F j, Y g:ia'),
              '@segments' => implode(', ', $included_segments),
              '@recipients' => $response['recipients'],
            ));

            \Drupal::logger('onesignal_pushout')->notice($message);
            \Drupal::messenger()->addMessage($message, 'status', FALSE);
          } else {
            \Drupal::logger('onesignal_pushout')->notice($response);
            \Drupal::messenger()->addMessage($response, 'error', FALSE);
          }
        } else {
          //TODO: No one to send to
        }
      }
    } else {
      $message = 'Missing OneSignal App ID and API Key! Enter the App ID and API Key in the @onesignal_settings.';
      $url = Url::fromRoute('onesignal_pushout.settings');

      $message = t($message, [
        '@onesignal_settings' => Link::fromTextAndUrl(t('OneSignal settings'), $url)->toString(),
      ]);

      \Drupal::logger('onesignal_pushout')->notice($message);
      \Drupal::messenger()->addMessage($message, 'error', FALSE);
    }
  }
}
