<?php

namespace Drupal\onesignal_pushout\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Upanupstudios\OneSignal\Php\Client\Config;
use Upanupstudios\OneSignal\Php\Client\OneSignal;
use GuzzleHttp\Client;
use Drupal\redirect\Entity\Redirect;

/**
 * Plugin implementation of the 'onesignal_sms' field type.
 *
 * @FieldType(
 *   id = "onesignal_sms",
 *   label = @Translation("OneSignal SMS"),
 *   description = @Translation("Allows an entity to send an SMS through OneSignal service."),
 *   default_widget = "onesignal_sms_default"
 * )
 */
class OneSignalSMSFieldItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return array(
    ) + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return array(
      'included_segments' => '',
      'select_segments' => FALSE,
      'sendnow_label' => 'Immediately send SMS',
      'sms_header' => '',
      'sms_from' => '',
      'sms_short_url' => TRUE
    ) + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['recipients'] = DataDefinition::create('integer')
      ->setLabel(t('Recipients'))
      ->setDescription(t('Number of SMS recipients.'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'recipients' => [ //TODO: Change to number of recipient
          'type' => 'int',
          'size' => 'normal',
          'not null' => TRUE,
          'default' => 0,
        ]
      ]
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $element = parent::storageSettingsForm($form, $form_state, $has_data);

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::fieldSettingsForm($form, $form_state);

    $settings = \Drupal::config('onesignal_pushout.settings');
    $api_key = $settings->get('api_key');

    if(!empty($api_key)) {
      $onesignal_url = Url::fromUri('https://app.onesignal.com/apps', array('attributes' => array('target' => '_blank')));

      $element['included_segments'] = array(
        '#type' => 'textarea',
        '#title' => $this->t('Included Segments'),
        '#multiple' => TRUE,
        '#description' => $this->t('List the segment names you want to target. One segment name per line. Users in these segments will receive a notification. Manage segments in @OneSignal.', [
          '@OneSignal' => Link::fromTextAndUrl('OneSignal', $onesignal_url)->toString()
        ]),
        '#default_value' => $this->getSetting('included_segments'),
        '#required' => TRUE,
      );
      $element['select_segments'] = array(
        '#title' => 'Enable to select segments (from included segments) before sending',
        '#type' => 'checkbox',
        '#default_value' => $this->getSetting('select_segments'),
      );
      $element['sendnow_label'] = array(
        '#title' => 'Send now label',
        '#type' => 'textfield',
        '#default_value' => $this->getSetting('sendnow_label'),
      );
      $element['sms_header'] = array(
        '#title' => 'SMS Header',
        '#type' => 'textfield',
        '#default_value' => $this->getSetting('sms_header'),
        '#description' => $this->t('Optional header text'),
      );
      $element['sms_from'] = array(
        '#title' => 'SMS From',
        '#type' => 'textfield',
        '#default_value' => $this->getSetting('sms_from'),
        '#description' => $this->t('Phone Number used to send SMS. Should be a registered Twilio phone number in E.164 format. For example: +11234567890.'),
        '#required' => TRUE
      );
      $element['sms_short_url'] = array(
        '#title' => 'Use short URLs',
        '#type' => 'checkbox',
        '#default_value' => $this->getSetting('sms_short_url'),
        '#description' => $this->t('Create a redirect with a short URL path for the entity.'),
      );
    } else {
      $message = 'Missing OneSignal API key! Enter the API information in the @onesignal_pushout_settings.';
      $url = Url::fromRoute('onesignal_pushout.settings');

      $message = $this->t($message, [
        '@onesignal_pushout_settings' => Link::fromTextAndUrl($this->t('OneSignal settings'), $url)->toString(),
      ]);

      \Drupal::logger('onesignal_pushout')->notice($message);
      \Drupal::messenger()->addMessage($message, 'error', FALSE);
    }

    return $element;
  }

  //TODO: Validate fieldSettingsForm
  // Calidate sms_from with proper format

  /**
   * {@inheritdoc}
   */
  public function postSave($update) {
    parent::postSave($update);

    // Get onesignal settings
    $settings = \Drupal::config('onesignal_pushout.settings');
    $app_id = $settings->get('app_id');
    $api_key = $settings->get('api_key');
    $debug_sendto_segment = $settings->get('debug_sendto_segment');

    if(!empty($app_id) && !empty($api_key)) {
      $config = new Config($app_id, $api_key);
      $httpClient = new Client();
      $oneSignal = new OneSignal($config, $httpClient);

      $entity = $this->getEntity();
      $isPublished = $entity->isPublished();

      if($isPublished && !empty($this->sendnow)) {
        // Entity
        $entity = clone $this->getEntity();

        if(!empty($debug_sendto_segment)) {
          // Use $debug_sendto_segment
          $included_segments = array($debug_sendto_segment);
        } else {
          // Retrieve included segments
          $included_segments = array_filter(explode("\n", $this->getSetting('included_segments')));
        }

        //TODO: Figure out using filters later on, keep empty for now
        $filters = [];

        // Only send if either targets are not empty
        if(!empty($included_segments) || !empty($filters)) {
          $datetime = new \DateTime('now');
          $header = $this->getSetting('sms_header');
          $from = $this->getSetting('sms_from');
          $short_url = $this->getSetting('sms_short_url');

          // What if there's an alias already?
          $url = $entity->toUrl('canonical', ['absolute' => TRUE])->toString();

          if(preg_match('/\/node\/[\d]+/', $url)) {
            // Clean the string
            $alias = \Drupal::service('pathauto.alias_cleaner')->cleanString($entity->getTitle());

            // Make unique
            $internalPath = $entity->toUrl()->getInternalPath();
            $source = '/' . $internalPath;
            $langcode = $entity->language()->getId();

            \Drupal::service('pathauto.alias_uniquifier')->uniquify($alias, $source, $langcode);

            // Get the pattern for the entity
            $pattern = \Drupal::service('pathauto.generator')->getPatternByEntity($entity);

            if(!empty($pattern)) {
              // Replace the token to get path
              $path = str_replace('[node:title]', $alias, $pattern->getPattern());

              // Generate URL from path
              $url = Url::fromUserInput($path, ['absolute' => TRUE])->toString();
            } else {
              // Generate URL from node ID
              $url = $entity->toUrl('canonical', ['absolute' => TRUE])->toString();
            }
          }

          if(!empty($short_url)) {
            // Get the path and create a 6 character hash
            $request = \Drupal::request();
            $baseUrl = $request->getSchemeAndHttpHost();
            $source = substr(md5($entity->toUrl()->toString()), 0, 6);

            // Check for existing redirects with
            $repository = \Drupal::service('redirect.repository');
            $redirects = $repository->findBySourcePath($source);

            if(empty($redirects)) {
              // Create redirect
              $redirectData = [
                'redirect_source' => $source,
                'redirect_redirect' => 'internal:/node/'.$entity->id(),
                'status_code' => 301,
              ];

              // Returns TRUE
              $redirect = Redirect::create($redirectData)
                ->save();

              if(empty($redirect)) {
                // If the redirect was not created, revert the source to the original path
                $source = substr($entity->toUrl()->toString(), 1);
              }
            }

            // Prepend baseUrl
            $url = $baseUrl.'/'.$source;

            // Get rid of www.
            $url = str_replace('www.', '', $url);
          }

          $content = $this->t('@header @title - @url', [
            '@header' => $header,
            '@title' => $entity->getTitle(),
            '@url' => $url
          ]);
          $content = trim($content->__toString());

          $data = [
            'app_id' => $app_id,
            'name' => $entity->getTitle(),
            'sms_from' => $from,
            'contents' => [
              'en' => $content
            ],
            'included_segments' => $included_segments,
            //TODO: Do not pass filters, either segments or filters should be passed, not both
            //'filters' => $filters,
            // Send with GMT to offset time in API (API defaults to UTC)
            'send_after' => $datetime->format('Y-m-d H:i:s P'),
          ];

          $response = $oneSignal->notifications()->create($data);

          if(!empty($response['id'])) {
            if(!empty($this->sendnow)) {
              $message = 'The "@subject" has been sent to @segments segments recipients.';
            } else {
              $message = 'The "@subject" will be sent on @date to @segments segments recipients.';
            }

            $message = $this->t($message, array(
              '@subject' => $entity->getTitle(),
              '@date' => $datetime->format('F j, Y g:ia'),
              '@segments' => implode(', ', $included_segments),
            ));

            \Drupal::logger('onesignal_pushout')->notice($message);
            \Drupal::messenger()->addMessage($message, 'status', FALSE);
          } else {
            if(!empty($response['errors'])) {
              $response = implode(', ', $response['errors']);
            }

            \Drupal::logger('onesignal_pushout')->notice($response);
            \Drupal::messenger()->addMessage($response, 'error', FALSE);
          }
        }
      }
    } else {
      $message = 'Missing OneSignal App ID and API Key! Enter the App ID and API Key in the @onesignal_settings.';
      $url = Url::fromRoute('onesignal_pushout.settings');

      $message = t($message, [
        '@onesignal_settings' => Link::fromTextAndUrl(t('OneSignal settings'), $url)->toString(),
      ]);

      \Drupal::logger('onesignal_pushout')->notice($message);
      \Drupal::messenger()->addMessage($message, 'error', FALSE);
    }
  }
}
