<?php

namespace Drupal\onesignal_pushout\Plugin\Field\FieldWidget;

use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Plugin implementation of the 'onesignal_sms_default' widget.
 *
 * @FieldWidget(
 *   id = "onesignal_sms_default",
 *   label = @Translation("SMS"),
 *   field_types = {
 *     "onesignal_sms"
 *   }
 * )
 */
class OneSignalSMSWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $settings = \Drupal::config('onesignal_pushout.settings');
    $api_key = $settings->get('api_key');
    $debug_sendto_segment = $settings->get('debug_sendto_segment');

    if(!empty($api_key)) {
      $description = $this->fieldDefinition->getDescription();

      if(!empty($debug_sendto_segment)) {
        $message = 'SMS will be sent to @segment segment. Change in @onesignal_settings.';
        $url = Url::fromRoute('onesignal_pushout.settings');

        $message = t($message, [
          '@segment' => $debug_sendto_segment,
          '@onesignal_settings' => Link::fromTextAndUrl(t('OneSignal settings'), $url)->toString(),
        ]);

        \Drupal::messenger()->addMessage($message, 'warning', FALSE);
      }

      $element['sendnow'] = [
        '#title' => $this->getFieldSetting('sendnow_label') ?: $this->t('Immediately send SMS'),
        '#description' => $description,
        '#type' => 'checkbox',
        '#default_value' => FALSE
      ];
    }

    return $element;
  }
}