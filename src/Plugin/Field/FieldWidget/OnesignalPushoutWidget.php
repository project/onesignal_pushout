<?php

namespace Drupal\onesignal_pushout\Plugin\Field\FieldWidget;

use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Plugin implementation of the 'onesignal_pushout_default' widget.
 *
 * @FieldWidget(
 *   id = "onesignal_pushout_default",
 *   label = @Translation("Pushout"),
 *   field_types = {
 *     "onesignal_pushout"
 *   }
 * )
 */
class OnesignalPushoutWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $settings = \Drupal::config('onesignal_pushout.settings');
    $api_key = $settings->get('api_key');
    $debug_sendto_segment = $settings->get('debug_sendto_segment');

    if(!empty($api_key)) {
      if(!empty($debug_sendto_segment)) {
        $message = 'Push notifications will be sent to @segment segment. Change in @onesignal_settings.';
        $url = Url::fromRoute('onesignal_pushout.settings');

        $message = t($message, [
          '@segment' => $debug_sendto_segment,
          '@onesignal_settings' => Link::fromTextAndUrl(t('OneSignal settings'), $url)->toString(),
        ]);

        \Drupal::messenger()->addMessage($message, 'warning', FALSE);
      }

      $element['pushnow'] = [
        '#title' => $this->getFieldSetting('pushnow_label') ?: $this->t('Immediately send push notifications'),
        '#type' => 'checkbox',
        '#default_value' => FALSE,
        '#attributes' => [
          'class' => [
            'pushnow-checkbox',
          ],
        ]
      ];

      if(!empty($this->getFieldSetting('scheduled_pushout'))) {
        $element['pushlater'] = [
          '#title' => $this->getFieldSetting('scheduled_pushout_label') ?: $this->t('Schedule sending push notifications'),
          '#type' => 'checkbox',
          '#default_value' => FALSE,
          '#attributes' => [
            'class' => [
              'pushlater-checkbox',
            ],
          ],
          '#attached' => [
            'library' => [
              'onesignal_pushout/onesignal_pushout_widget'
            ]
          ]
        ];

        $element['pushlater_datetime'] = [
          '#title' => $this->t('Date &amp; time'),
          '#type' => 'datetime',
          '#attributes' => [
            'class' => [
              'pushlater-datetime',
            ],
          ],
          '#element_validate' => [
            [static::class, 'validate'],
          ],
        ];
      }
    }

    return $element;
  }

  /**
   * Validate the datetime field.
   */
  public static function validate($element, FormStateInterface $form_state) {
    // Get the field name
    $field_name = current($element['#parents']);

    // Get value
    $value = $form_state->getValue($field_name);

    if(!empty($value[0]['pushlater'])) {
      $pushlater_datetime = array_filter($value[0]['pushlater_datetime']);

      if(!empty($pushlater_datetime) && !empty($pushlater_datetime['date']) && !empty($pushlater_datetime['time'])) {
        // Check if it's greater than the date and time now
        // Note: the site's timezone is used to convert the time
        $now = new DrupalDateTime();
        $datetime = $pushlater_datetime['object'];

        if($datetime < $now) {
          $form_state->setError($element, $this->t("Schedule must be set in the future."));
        }
      } else {
        $form_state->setError($element, $this->t("Enter scheduled date and time."));
      }
    }
  }

}